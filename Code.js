/**
 * Copyright  National Citizen Service Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Deployment Notify')
      .addItem('Send Depoyment Notification', 'sendDepoymentNotification')
      .addToUi();
}



var EMAIL_SUBJECT = 'Deployment Status for Release';

/**
 * A global constant 'notice' text to include with each email
 * notification.
 */
var NOTICE = 'Notifications was created as part of Salesforce Delivery and Deployment Status';



//Get a list of feature for a deployment from sheet
function getReleaseFeatureList(){
var ss = SpreadsheetApp.openById('1w_qLUHi9Yse4NOhoZkYEqFCBC5oeanqN7F-TpYSpp8s');
var dataSheet = ss.getSheetByName('Deployment Readiness');
var dataRange = dataSheet.getDataRange();
var dataValues = dataRange.getValues();

var table = [['Key', 'Summary', 'Status']];

  for (var i = 1; i < dataValues.length; i++) {
    table.push([dataValues[i][0], dataValues[i][1],dataValues[i][2]]);
  }

  Logger.log('Feature Table List: '+ table);
 return table;
}


function getStakeholderAudienceList(){
  var ss = SpreadsheetApp.openById('1w_qLUHi9Yse4NOhoZkYEqFCBC5oeanqN7F-TpYSpp8s');

var dataSheet = ss.getSheetByName('Stakeholder Audience');
//var dataRange = dataSheet.getRange(2, 1, dataSheet.getMaxRows() - 1, 4);

var dataRange = dataSheet.getDataRange();
var dataValues = dataRange.getValues();
var audienceList = [];


  for (var i = 1; i < dataValues.length; i++) {
        audienceList.push([dataValues[i][0], dataValues[i][3]]);
        Logger.log(dataValues[i][0] + ' == ' + dataValues[i][3]);
    }
    
  Logger.log('audienceList == ' + JSON.stringify(audienceList));
  return audienceList;
}





function createReleaseDocument(){
   
var copyDocId = DriveApp.getFileById('1v66iXJp7g7IEERMoqOTRR1IyzqAoOW-RR9Png1qnL8Q').makeCopy('Salesforce Release Deployment Document').getId();
   getReleaseTrackerData();

   Logger.log('copyDocId ==' + copyDocId);
  var newCopyDoc =  DocumentApp.openById(copyDocId);
  var featureListTable = getReleaseFeatureList();
  var documentProperties = PropertiesService.getDocumentProperties();

  newCopyDoc.getBody().appendParagraph("Feature List");
  newCopyDoc.getBody().appendPageBreak();
  newCopyDoc.getBody().appendTable(featureListTable);


  newCopyDoc.getBody().replaceText("{!DEPLOYMENT_DATE}", documentProperties.getProperty('DEPLOYMENT_DATE'));
  //newCopyDoc.getBody().replaceText("{!APPROVED_BY}", documentProperties.getProperty('APPROVED_BY'));
  newCopyDoc.getBody().replaceText("{!APPROVED_DATE}", documentProperties.getProperty('APPROVED_DATE'));
  newCopyDoc.getBody().replaceText("{!DEPLOYER}", documentProperties.getProperty('DEPLOYER'));
  newCopyDoc.getBody().replaceText("{!REVIEWER}", documentProperties.getProperty('REVIEWER'));
  newCopyDoc.getBody().replaceText("{!RELEASE_VERSION}", documentProperties.getProperty('RELEASE_VERSION'));
  newCopyDoc.getBody().replaceText("{!RELEASE_BRANCH}", documentProperties.getProperty('RELEASE_BRANCH'));

  newCopyDoc.saveAndClose();

   return newCopyDoc;
}


function createEmailTemplate(){
     var template = HtmlService.createTemplateFromFile('deploymentNotification');
      template.summary = 'Testing Summary';
      template.version = 'Template Resposse';
      template.releaseBranch = 'Test email Sending ';
      template.releaseContentDetails = 'Test response Step';
      template.sheetUrl = 'https://docs.google.com/spreadsheets/d/13ivC3rR-uqhp-Szt4bimFnK4E59I1eXApRLQm9SP9FQ/edit#gid=2116749267';
      template.notice = NOTICE;
      var message = template.evaluate();

    Logger.log(message);

      return message;
}
/**
 * Sends emails from the Stakeholder Audience List spreadsheet.
 */
function sendDepoymentNotification() {
  var message = createEmailTemplate();
  var releaseDoc = createReleaseDocument();
  var audienceList = getStakeholderAudienceList();

  for (var i = 0; i < audienceList.length; ++i) {
      var emailAddress = audienceList[i][1];

   Logger.log('emailAddress == ' + emailAddress);

     MailApp.sendEmail(emailAddress,
          'Deployment Status for Release - Release V52.6.1',
          message.getContent(), {
            name: 'Salesforce Delivery Team',
            htmlBody: message.getContent(),
            attachments: releaseDoc.getAs(MimeType.PDF)
          });    

  }

  //update release-tracker sheet to update email sent
   updateReleaseTrackerSheet();

  //clean Document Properties
  PropertiesService.getDocumentProperties().deleteAllProperties;
}

function updateReleaseTrackerSheet(){
 var ss = SpreadsheetApp.openById('1w_qLUHi9Yse4NOhoZkYEqFCBC5oeanqN7F-TpYSpp8s');
  var dataSheet = ss.getSheetByName('ReleaseTracker');
  var documentProperties = PropertiesService.getDocumentProperties();


 var updateEmailSentCell = documentProperties.getProperty('EMAIL_SENT');
  //update email sent cell for YES.
  Logger.log('updateEmailSentCell == ' + updateEmailSentCell);
  dataSheet.getRange(updateEmailSentCell).setValue('YES');

  SpreadsheetApp.flush();
}

 function getReleaseTrackerData(){
  var ss = SpreadsheetApp.openById('1w_qLUHi9Yse4NOhoZkYEqFCBC5oeanqN7F-TpYSpp8s');
  var dataSheet = ss.getSheetByName('ReleaseTracker');
  var dataRange = dataSheet.getDataRange();
  var dataValues = dataRange.getValues();

  var documentProperties = PropertiesService.getDocumentProperties();


  for (var i = 1; i < dataValues.length; i++) {
        
       if(dataValues[i][8] !== 'YES'){
               Logger.log(JSON.stringify(dataValues[i]));
            var emailSentCell = 'J'+i;
      	    documentProperties.setProperty('DEPLOYMENT_DATE', dataValues[i][0]);
            documentProperties.setProperty('APPROVED_BY ', dataValues[i][3]);
            documentProperties.setProperty('APPROVED_DATE', dataValues[i][4]);
            documentProperties.setProperty('DEPLOYER', dataValues[i][5]);
            documentProperties.setProperty('REVIEWER', dataValues[i][6]);
            documentProperties.setProperty('RELEASE_VERSION', dataValues[i][2]);
            documentProperties.setProperty('RELEASE_BRANCH', dataValues[i][9]);
            documentProperties.setProperty('EMAIL_SENT', emailSentCell);
       }
       
    }

 }






 